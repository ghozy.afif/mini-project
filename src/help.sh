function help()
{

cat << EOF

Usage: prosa [options]

Options:
  init                              Bootstraping prosa development tools
  ci                                Manage CI manifest
  project                           Manage project monorepo
  os                                OS information
  version                           Version information
  remote                            Remote staging environment

EOF

}