#!/bin/bash

function ci()
{
    if [ ! -n "$1" ]
    then
    ./prosa ci help
    exit
    fi

    while [ -n "$1" ]; do

        case "$1" in

        generate-staging) ci-generate-staging $2;;

        generate-production) ci-generate-production $2;;

        generate-test) ci-generate-test $2;;

        help) ci-help ;;

        #*) help ;;

        esac

        shift

    done
}

function ci-generate-test()
{

if [ ! -n "$1" ]
then
    ./prosa ci help
    exit
fi

if [ ! -d "$1" ]
then
    echo "Your project directory is not found!"
    exit
fi

SVC=$1

echo "Add $SVC TEST to .gitlab-ci.yml"

cat << EOF >> .gitlab-ci.yml
##### BEGIN TEST definition for '$SVC'#####

test-$SVC:
    stage: test
    tags: 
        - ansible-staging
    only:
        refs:
            - merge_requests
        changes:
            - $SVC/**/*
        variables:
            - \$CI_MERGE_REQUEST_TITLE !~ /WIP/
    script:
        - cd $SVC
        - docker run -u $(id -u ${USER}):$(id -g ${USER}) -i -v \$PWD:/usr/src sonarsource/sonar-scanner-cli sonar-scanner   -Dsonar.projectKey=$SVC   -Dsonar.sources=.   -Dsonar.host.url=\$SONAR_URL   -Dsonar.login=\$SONAR_TOKEN

##### END TEST definition for '$SVC' #####
EOF
}

function ci-generate-staging()
{

if [ ! -n "$1" ]
then
    ./prosa ci help
    exit
fi

if [ ! -d "$1" ]
then
    echo "Your project directory is not found!"
    exit
fi

SVC=$1

echo "Add $SVC CI definition STAGING to .gitlab-ci.yml"

cat << EOF >> .gitlab-ci.yml
##### BEGIN build definition for '$SVC' staging #####

build-$SVC-staging:
    stage: build
    tags: 
        - runner1
    only:
        refs:
            - merge_requests
        changes:
            - $SVC/**/*
        variables:
            - \$CI_MERGE_REQUEST_TITLE !~ /WIP/
    script:
        - cd $SVC
        - docker build -t \$CONTAINER_BASE/$SVC:latest .
        - docker push \$CONTAINER_BASE/$SVC:latest

deploy-$SVC-staging:
    stage: deploy
    tags: 
        - runner1
    only:
        refs:
            - merge_requests
        changes:
            - $SVC/**/*
        variables:
            - \$CI_MERGE_REQUEST_TITLE !~ /WIP/
    script:
        - cd $SVC
        - cd ..
        - sh deploy.sh "$SVC" "latest" "staging"

##### END build definition for '$SVC'  staging #####
EOF
}

function ci-generate-production()
{

if [ ! -n "$1" ]
then
    ./prosa ci help
    exit
fi

if [ ! -d "$1" ]
then
    echo "Your project directory is not found!"
    exit
fi

SVC=$1

echo "Add $SVC CI definition PRODUCTION to .gitlab-ci.yml"

cat << EOF >> .gitlab-ci.yml
##### BEGIN build definition for '$SVC' production #####

build-$SVC-production:
    stage: build
    tags: 
        - runner1
    only:
        refs:
            - master
        changes:
            - $SVC/version
        variables:
            - \$SERVICE_NAME == null
    script:
        - cd $SVC
        - VERSION=\`cat version\` docker build -t \$CONTAINER_BASE/$SVC:prod-\$CI_COMMIT_SHA --build-arg BUILD_NUMBER=\$CI_COMMIT_SHORT_SHA --build-arg VERSION=\$VERSION .
        - docker push \$CONTAINER_BASE/$SVC:prod-\$CI_COMMIT_SHA

release-$SVC-production:
    stage: release
    tags: 
        - runner1
    only:
        refs:
            - master
        changes:
            - $SVC/version
        variables:
            - \$SERVICE_NAME == null
    script:
        - VERSION=\`cat $SVC/version\`
        - docker pull \$CONTAINER_BASE/$SVC:prod-\$CI_COMMIT_SHA
        - docker tag \$CONTAINER_BASE/$SVC:prod-\$CI_COMMIT_SHA \$CONTAINER_BASE/$SVC:\$VERSION
        - docker push \$CONTAINER_BASE/$SVC:\$VERSION

deploy-$SVC-production:
    stage: deploy
    tags: 
        - runner1
    only:
        refs:
            - master
        changes:
            - $SVC/version
        variables:
            - \$SERVICE_NAME == null
    script:
        - VERSION=\`cat $SVC/version\`
        - cd $SVC
        - cd ..
        - sh deploy.sh "$SVC" "\$VERSION" "production"

##### END build definition for '$SVC' production #####
EOF
}

function ci-help()
{

cat << EOF

Usage: prosa ci [options]

Options:
  generate-staging [PROJECT NAME]                          Create build definition for staging universe
  generate-production [PROJECT NAME]                       Create build definition for production universe
  generate-test [PROJECT NAME]                             Create TEST definition
EOF

}