function osx()
{
    if ! which brew >/dev/null;then
        echo "Please install 'brew' first";
        echo '/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"'
        exit
    fi
    # if ! which docker >/dev/null;then
    #     echo "Please install 'docker' first";
    # fi
    # echo "Installing nodejs 10..."
    # brew install node@10
    # node --version
    # echo "Installing orcinus..."
    # npm install -g orcinus --unsafe-perm
    # echo "Setup mini cluster environment..."
    # orcinus cluster init 127.0.0.1
    # orcinus cluster ls
    # echo "Done...."
    echo "Setup npm config"
    npm config set @mayarid:registry https://gitlab.com/api/v4/projects/20099162/packages/npm/
    npm config set -- '//gitlab.com/api/v4/projects/20099162/packages/npm/:_authToken'  "p2fX_Rt4bVYxW_MvcXCS"
    if ! which go >/dev/null;then
        echo "Setup golang env"
        brew install golang
    fi
    if ! which node >/dev/null;then
        echo "Setup nodejs env"
        curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash
        source ~/.bashrc && nvm install 12
    fi
}

function debian()
{
    # echo "Installing nodejs 10..."
    # curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
    # sudo apt-get install -y nodejs
    # node --version
    # sudo apt-get install -y build-essential
    # if ! which docker >/dev/null;then
    #     sudo apt-get install \
    #     apt-transport-https \
    #     ca-certificates \
    #     curl \
    #     gnupg-agent \
    #     software-properties-common
        
    #     curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

    #     sudo apt-key fingerprint 0EBFCD88

    #     sudo add-apt-repository \
    #     "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    #     $(lsb_release -cs) \
    #     stable"

    #     sudo apt-get update
    #     sudo apt-get install docker-ce docker-ce-cli containerd.io

    # fi
    # sudo systemctl enable docker
    # sudo systemctl start docker
    # sudo usermod -aG docker $USER

    # echo "Installing orcinus..."
    # sudo npm install -g orcinus --unsafe-perm
    # echo "Setup mini cluster environment..."
    # sg docker -c "orcinus cluster init 127.0.0.1"
    # sg docker -c "orcinus cluster ls"
    # echo "Done...."
    echo "Setup npm config"
    npm config set @mayarid:registry https://gitlab.com/api/v4/projects/20099162/packages/npm/
    npm config set -- '//gitlab.com/api/v4/projects/20099162/packages/npm/:_authToken'  "p2fX_Rt4bVYxW_MvcXCS"
}

function redhat()
{
    # echo "Installing nodejs 10..."
    # curl -sL https://rpm.nodesource.com/setup_10.x | sudo bash -
    # sudo yum install -y gcc-c++ make nodejs
    # if ! which docker >/dev/null;then
    #     sudo dnf -y install dnf-plugins-core
    #     sudo dnf config-manager \
    #         --add-repo \
    #         https://download.docker.com/linux/fedora/docker-ce.repo
    #     sudo dnf install docker-ce docker-ce-cli containerd.io
    # fi
    # sudo systemctl enable docker
    # sudo systemctl start docker
    # sudo usermod -aG docker $USER

    # echo "Installing orcinus..."
    # sudo npm install -g orcinus --unsafe-perm
    # echo "Setup mini cluster environment..."
    # sg docker -c "orcinus cluster init 127.0.0.1"
    # sg docker -c "orcinus cluster ls"
    # echo "Done...."
    echo "Setup npm config"
    npm config set @mayarid:registry https://gitlab.com/api/v4/projects/20099162/packages/npm/
    npm config set -- '//gitlab.com/api/v4/projects/20099162/packages/npm/:_authToken'  "p2fX_Rt4bVYxW_MvcXCS"
}

function init()
{
    if [ "${1}" = "OSX" ]; then
    osx
    elif [ "${1}" = "DEBIAN" ]; then
    debian
    elif [ "${1}" = "REDHAT" ]; then
    redhat
    fi

}
