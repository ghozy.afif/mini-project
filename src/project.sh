#!/bin/bash

function project()
{
    if [ ! -n "$1" ]
    then
    ./prosa project help
    exit
    fi

    while [ -n "$1" ]; do

        case "$1" in

        new) new $@;;

        deploy) deploy $@;;

        rm) remove $@;;

        ps) process $@;;

        inspect) inspect $@;;

        logs) logs $@;;

        node) source ~/.bashrc && nvm install $2 ;;

        help) project-help ;;

        esac

        shift

    done
}

function new()
{
OS=`uname -s`
REV=`uname -r`
MACH=`uname -m`
DIST=""

if [ "${OS}" = "Darwin" ]; then
    DIST="OSX"
elif [ "${OS}" = "Linux" ] ; then
    KERNEL=`uname -r`
    if [ -f /etc/redhat-release ] ; then
        DIST='REDHAT'
    elif [ -f /etc/debian_version ] || [ -d "/data/data/com.termux/files/home" ]; then
        DIST='DEBIAN'
    else
        echo "bitch!!!"
        exit
    fi
else
    echo "Kill your self, please install supported OS, Ubuntu/Debian, Fedora/Centos, OSX!!!"
    exit
fi

if [ ! -n "$2" ]
then
    echo "Please set the name of your service!"
    exit
fi

PROJECT=${@/new/}
SERVICE=${PROJECT/ /}

cat << EOF

Boiler plate list :
1. Blank project
2. Python service project
3. Nodejs Library
4. Queue service
EOF

echo -n "Select the boilerplate (Press Ctrl+c for cancel) : "
read BOILERPLATE 

case "$BOILERPLATE" in

    1)  if [ -d "$2" ]
        then
            echo "The project name has been exist!"
            exit
        fi

        cp -rf boilerplate/blank $SERVICE; echo "Your project \`$SERVICE\` has been created ($PWD/$SERVICE)";;

    2)  if [ -d "svc-$2" ]
        then
            echo "The project name has been exist!"
            exit
        fi

        cp -rf boilerplate/python svc-$SERVICE;;

    3)  if [ -d "libs/js/$SERVICE" ]
        then
            echo "The javascript library name has been exist!"
            exit
        fi
        cp -rf boilerplate/js-libs libs/js/$SERVICE;
        if [ "$DIST" != "OSX" ]
        then
            sed -i "s|<SERVICE>|$SERVICE|g" libs/js/$SERVICE/package.json
            echo "Your service \`$SERVICE\` has been created ($PWD/libs/js/$SERVICE)"
        else
            sed -i "" "s|<SERVICE>|$SERVICE|g" libs/js/$SERVICE/package.json
            echo "Your service \`$SERVICE\` has been created ($PWD/libs/js/$SERVICE)"
        fi ;;
    4)  SERVICE_DIR=queue-$SERVICE;
        if [ -d "$SERVICE_DIR" ]
        then
            echo "The queue service name has been exist!"
            exit
        fi
        cp -rf boilerplate/queue $SERVICE_DIR;

        if [ "$DIST" != "OSX" ]
        then
            sed -i "s|<SERVICE>|$SERVICE_DIR|g" $SERVICE_DIR/package.json
            sed -i "s|<SERVICE>|$SERVICE_DIR|g" $SERVICE_DIR/server.js
            echo "Your service \`$SERVICE_DIR\` has been created ($PWD/$SERVICE_DIR)"
        else
            sed -i "" "s|<SERVICE>|$SERVICE_DIR|g" $SERVICE_DIR/package.json
            sed -i "" "s|<SERVICE>|$SERVICE_DIR|g" $SERVICE_DIR/server.js
            echo "Your service \`$SERVICE_DIR\` has been created ($PWD/$SERVICE_DIR)"
        fi ;;

    *) ./prosa project $@ ;;

esac

}

function deploy()
{
    PROJECT=${@/deploy/}
    SERVICE=${PROJECT/ /}
    echo "Deploy '${SERVICE}' to your local env!"
    docker build -t ${SERVICE}:dev ${SERVICE}
    echo -----------------------------------------
}

function remove()
{
    PROJECT=${@/rm/}
    SERVICE=${PROJECT/ /}
    echo "Remove ${SERVICE} project from your local environment!"
}

function process()
{
    PROJECT=${@/ps/}
    SERVICE=${PROJECT/ /}
    echo "Get process from ${SERVICE} project!"
}

function inspect()
{
    PROJECT=${@/inspect/}
    SERVICE=${PROJECT/ /}
    echo "Inspect your config from ${SERVICE} project!"
}

function logs()
{
    PROJECT=${@/logs/}
    SERVICE=${PROJECT/ /}
    echo "Get logs from ${SERVICE} project!"
    orcinus logs follow ${SERVICE}
}

function project-help()
{

cat << EOF

Usage: prosa project [options] PROJECT_NAME

Options:

    new                             Create new project
    deploy                          Deploy your project to local environment
    rm                              Remove your project container from local environment
    ps                              List process your container service
    inspect                         Inspect confic your container service
    logs                            Get logs container

EOF

}