const queue = require('@mayarid/queue')('<SERVICE>');
if (!process.env.MAYAR_REDIS_HOST) {
    require("dotenv").config({ path: "env" });
}

const worker = (job, done) => {
    console.log('processing job ' + job.id);
    setTimeout(function () {
        console.log("result :" + job.data.x + job.data.y)
        done(null, job.data.x + job.data.y);
    }, 10);
}

queue.on('ready', () => {
    queue.process((job, done) => {
        worker(job, done)
    });

    console.log('processing jobs...');
});