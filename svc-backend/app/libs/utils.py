import requests
import json
import os
import logging
import coloredlogs

import datetime
import html
import math
import random
import re
from urllib.parse import urljoin, urlparse

import pytz
from bs4 import BeautifulSoup
from flask import request, url_for
from pathlib import Path

clean_regex = re.compile("<.*?>")
FORCE_HTTPS_RESOURCE = os.getenv("FORCE_HTTPS_RESOURCE", "false").lower() == "true"
PREFIX_PATH = os.getenv("PREFIX_PATH", "")
XLSX_ENGINE = "xlsxwriter"
XLS_ENGINE = "xlwt"
JSON_ENGINE = "json"
DEFAULT_SHEET = "Sheet_1"


def get_project_root() -> Path:
    return Path(__file__).parent.parent.parent


def handle_duplicate(e):
    detail = e.args[0].split("\n")[1]
    match = re.match(r"DETAIL:\s+Key\s+\((.*?)\)=\((.*?)\).*", detail)

    return {
        "message": f"Instance with {match.group(1)} = '{match.group(2)}' is already exists",
        "instance": match.group(1),
    }


def get_now_str():
    d = datetime.datetime.now(pytz.timezone("Asia/Jakarta"))
    return d.strftime("%B %d, %Y %X")


def get_alphanumeric(str_in):
    """
    Get lowercased alphanumeric of str_in (without white character)
    """
    str_lower = str_in.lower().strip()
    retval = ""
    for c in str_lower:
        if c.isalnum():
            retval += c
    return retval


def get_text_from_raw_html(str_in):
    """
    Strip all HTML tag from str_in, the text from each tag will be separated by space
    """
    soup = BeautifulSoup(str_in, features="lxml")
    text = soup.get_text(" ")
    return text


def replace_with_newlines(element):
    text = ""
    for elem in element.recursiveChildGenerator():
        if isinstance(elem, str):
            text += elem.strip()
        elif elem.name == "br":
            text += "\n"
    return text


def get_plain_text(str_in):
    soup = BeautifulSoup(str_in, features="lxml")
    plain_text = ""
    lines = soup.find("body")
    if lines:
        for line in lines.findAll("p"):
            line = replace_with_newlines(line)
            plain_text += line
    else:
        return str_in
    return plain_text


def adjust_html(str_in):
    """
    Change HTML tags in str_in so it can be rendered properly on the frontend
    """
    soup = BeautifulSoup(str_in, features="lxml")

    # If str_in contain oembed tag, convert it to iframe;
    # Any str_in containing oembed tag is assumed to contain a Youtube video,
    # so the a_list URL will also be converted into proper Youtube's embedded video link
    if soup.oembed is not None:
        url = soup.oembed["url"]
        url = url.replace("watch?v=", "embed/")

        soup.oembed.name = "iframe"
        soup.iframe["src"] = url
        del soup.iframe["url"]

        retval = soup.figure
    else:
        retval = soup.figure

    retval = str(retval)
    return retval


def clean_html(text, lowercased=False):
    text = re.sub(clean_regex, " ", text)
    text = html.unescape(text)
    if lowercased:
        text = text.lower()
    return " ".join(text.split())


def random_string(str_len=16):
    chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"
    randomstr = "".join((random.choice(chars)) for _ in range(str_len))
    return randomstr


def parse_boolean(text):
    if text and type(text) is str:
        lower_text = text.lower()
        if lower_text == "true":
            return True
        elif lower_text == "false":
            return False
    return False


def snake_upper_case_to_capital_each_word(text: str) -> str:
    result = text.replace("_", " ")
    result = result.title()
    return result


def file_format(file):
    content_type = file.content_type
    if content_type == "text/csv":
        return "csv"
    elif content_type == "application/vnd.ms-excel":
        return "xls"
    elif (
        content_type
        == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    ):
        return "xlsx"
    elif content_type == "application/octet-stream":
        file_ext = file.filename.split(".")[-1]
        if file_ext in ["csv", "xls", "xlsx"]:
            return file_ext
    return "other"


def slice_per(a_list, n_elem_in_group):
    """
    Slice a_list into sub continues list with max number of element in each group is n_elem_in_group
    """
    n_group = math.ceil(len(a_list) / n_elem_in_group)
    result = []
    l, r = 0, n_elem_in_group
    for i in range(n_group):
        result.append(a_list[l:r])
        l, r = r, r + n_elem_in_group
    return result


def generate_url(resource, **kwargs):
    if FORCE_HTTPS_RESOURCE:
        kwargs = {**kwargs, "_scheme": "https"}

    url = url_for(resource, **kwargs)

    prefix_path = PREFIX_PATH
    url_parse = urlparse(url)
    if prefix_path and not url_parse.path.startswith(prefix_path):
        listpath = [prefix_path, url_parse.path]
        new_path = "/" + "/".join(p.strip("/") for p in listpath)
        new_url = f"{url_parse.scheme}://{url_parse.netloc}{new_path}"
        if url_parse.query:
            new_url = f"{new_url}?{url_parse.query}"
        return new_url

    return url


def get_url_path(resource, **kwargs):
    url = url_for(resource, **kwargs)
    prefix_path = PREFIX_PATH
    if prefix_path and prefix_path in url:
        split = url.split(prefix_path)
        path = split[-1]
        if path[0] != "/":
            path = "/" + path
    else:
        url_parse = urlparse(url)
        path = url_parse.path
        if url_parse.query:
            path = f"{path}?{url_parse.query}"

    return path


def get_path_from_url(url):
    url_parse = urlparse(url)
    prefix_path = PREFIX_PATH
    path = url_parse.path
    if prefix_path and path.startswith(prefix_path):
        split = path.split(prefix_path, maxsplit=1)
        path = split[-1]
        if not path.startswith("/"):
            path = f"/{path}"
    if url_parse.query:
        path = f"{path}?{url_parse.query}"
    return path


def get_url_from_path(path):
    url = request.base_url
    return urljoin(url, path)


def generate_json(dataframe, output_stream):
    try:
        dataframe = dataframe.to_json(orient="records")
        output_stream.write(dataframe.encode())
        output_stream.seek(0)
    except Exception as e:
        raise (e)
    else:
        return output_stream


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


def log_rep(stdin):
    coloredlogs.install()
    logging.info(stdin)


def log_deb(stdin):
    coloredlogs.install()
    logging.debug(stdin)


def log_err(stdin):
    coloredlogs.install()
    logging.error(stdin)


def send_http(url, data, headers=None):
    json_data = json.dumps(data)
    try:
        send = requests.post(url, data=json_data, headers=headers)
        respons = send.json()
        response_time = send.elapsed.total_seconds()
    except requests.exceptions.RequestException:
        respons = {
            "result": False,
            "Error": "Failed to establish a new connection",
            "description": None,
        }
        return respons
    else:
        result = {
            "result": respons["result"],
            "time": response_time,
            "data": respons["data"],
            "status": respons["status"],
        }
        return result


def get_http(url, params=None):
    try:
        response = requests.get(url, params)
    except requests.exceptions.RequestException as e:
        log_err(e)
        return False
    except Exception as a:
        log_err(a)
        return False
    else:
        data = response.json()
        return data
