from app import db


class ActiveToken(db.Model):
    __tablename__ = "active_tokens"
    refresh_jti = db.Column(db.String(120), primary_key=True)
    access_jti = db.Column(db.String(120))
    user_id = db.Column(db.Integer, db.ForeignKey("users.id", ondelete="cascade"))
    can_expire = db.Column(db.Boolean, nullable=False, default=False, server_default="f")
    expire_date = db.Column(db.DateTime, nullable=True)

    def add(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def find_by_user_id(cls, user_id):
        return cls.query.filter_by(user_id=user_id)
