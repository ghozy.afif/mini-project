
from datetime import datetime

import pytz
from sqlalchemy import Column, DateTime, event
from sqlalchemy.ext.declarative import declarative_base


tz = pytz.timezone("Asia/Jakarta")


def gmt_now():
    return datetime.utcnow().replace(tzinfo=pytz.utc).astimezone(tz).replace(tzinfo=None)


class TimeStampMixin:
    """Timestamping mixin"""

    created_at = Column(DateTime, default=gmt_now)
    created_at._creation_order = 9998
    updated_at = Column(DateTime, default=gmt_now)
    updated_at._creation_order = 9998

    @staticmethod
    def _updated_at(mapper, connection, target):
        target.updated_at = gmt_now()

    @classmethod
    def __declare_last__(cls):
        event.listen(cls, "before_update", cls._updated_at)


class Base(TimeStampMixin):
    pass


Base = declarative_base(cls=Base)
