from app import db
from app.models import Base


class Message(db.Model, Base):
    __tablename__ = "messages"

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("users.id", ondelete="cascade"))
    content = db.Column(db.String(120), index=True, nullable=False)
    is_active = db.Column(db.Boolean, default=True, nullable=False, server_default="t")
    parent_id = db.Column(db.Integer, nullable=True)

    @property
    def user(self):
        return self.user

    @property
    def comments(self):
        messages = (
            Message.query.filter(Message.parent_id == self.id)
            .order_by(Message.created_at.asc())  # noqa: E712
            .order_by(Message.updated_at.asc())
            .paginate(int(1), int(3), False)
        )
        return messages.items
