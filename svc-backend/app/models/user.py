from datetime import datetime
from passlib.hash import pbkdf2_sha256 as sha256
import re
from sqlalchemy.ext.hybrid import hybrid_property
from typing import Any, Mapping, List

from app import db, redis_con
from app.models import Base
from app.models.active_token import ActiveToken
from app.libs.utils import random_string

EMAIL_REGEX = r"[^@]+@[^@]+\.[^@]+"
CHARACTER_PASSWORD_MIN = 5
CHARACTER_PASSWORD_MAX = 32


class IdentityKey:
    PASSWORD_RESET = "password-reset"
    EMAIL_VERIFICATION = "email-verification"
    REQUEST_EMAIL_VERIFICATION = "request-email-verification"


class User(db.Model, Base):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True, nullable=False)
    password = db.Column(db.Text, nullable=False)
    salt = db.Column(db.String(16), nullable=False, default="", server_default="")
    email = db.Column(db.String(120), index=True, unique=True, nullable=False)
    first_name = db.Column(db.String(64), nullable=False)
    last_name = db.Column(db.String(64), nullable=False)
    propic = db.Column(db.String(64), nullable=True)
    active_tokens = db.relationship(
        "ActiveToken", backref="user", cascade="all, delete"
    )
    messages = db.relationship("Message", backref="user", cascade="all, delete")
    is_active = db.Column(db.Boolean, default=True, nullable=False, server_default="t")

    def __repr__(self):
        return "<User {}>".format(self.username)

    def verify_password(self, password):
        return sha256.verify(password + self.salt, self.password)

    @staticmethod
    def generate_salt_and_hash(password):
        salt = random_string(16)
        return salt, sha256.hash(password + salt)

    @staticmethod
    def is_email_valid(email):
        return bool(re.match(EMAIL_REGEX, email))

    @staticmethod
    def is_password_valid(password):
        return (
            len(password) >= CHARACTER_PASSWORD_MIN
            and len(password) <= CHARACTER_PASSWORD_MAX
        )

    @property
    def name(self):
        return (self.first_name + " " + self.last_name).strip()

    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(username=username).first()

    @classmethod
    def find_by_email(cls, email):
        return cls.query.filter_by(email=email).first()

    def generate_hash(self, password):
        return sha256.hash(password + self.salt)

    # @property
    # def propic_url(self):
    #     if self.propic:
    #         return generate_url("userpropicget", filename=self.propic, _external=True)
    #     elif self.role.is_agent():
    #         return generate_url("staticimg", filename="agent.png", _external=True)
    #     else:
    #         return generate_url("staticimg", filename="user.png", _external=True)

    def generate_default_payload(self, key):
        return {"key": key, "username": self.username, "email": self.email}

    @property
    def full_name(self):
        return f"{self.first_name} {self.last_name}"

    def revoke_all_active_tokens(self):
        active_tokens = self.active_tokens
        now = datetime.now()
        for active_token in active_tokens:
            if not active_token.can_expire or active_token.expire_date >= now:
                redis_con.set(
                    active_token.access_jti,
                    active_token.access_jti,
                    exat=int(active_token.expire_date.timestamp()),
                )
        ActiveToken.find_by_user_id(self.id).delete()
        db.session.commit()

    def to_dict(self) -> Mapping[str, Any]:
        return {
            "id": self.id,
            "username": self.username,
            "email": self.email,
            "first_name": self.first_name,
            "last_name": self.last_name,
            "full_name": self.name,
            "is_active": self.is_active,
        }
