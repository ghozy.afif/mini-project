import os
from dotenv import load_dotenv
from app.libs import MetaFlaskEnv

APP_ROOT = os.path.join(os.path.dirname(__file__), '../..')
dotenv_path = os.path.join(APP_ROOT, '.env')
load_dotenv(dotenv_path)

POSTGRES_USERNAME = os.getenv("POSTGRES_USERNAME")
POSTGRES_PASSWORD = os.getenv("POSTGRES_PASSWORD")
POSTGRES_DATABASE = os.getenv("POSTGRES_DATABASE")
POSTGRES_HOST = os.getenv("POSTGRES_HOST")
POSTGRES_PORT = os.getenv("POSTGRES_PORT")
REDIS_HOST = os.getenv("REDIS_HOST")

class Config(MetaFlaskEnv):
    ENV_PREFIX = "FLASK_"
    SQLALCHEMY_DATABASE_URI = (
        f"postgresql://{POSTGRES_USERNAME}:{POSTGRES_PASSWORD}@"
        + f"{POSTGRES_HOST}:{POSTGRES_PORT}/{POSTGRES_DATABASE}"
    )
    JWT_SECRET_KEY = "l0urafr-9LIFXG1YwceySUA407IjRbwhaD1JT41x4tJXqY5EhL2gyphbvVWqn7Zy"
    JWT_ACCESS_TOKEN_EXPIRES = int(os.getenv("JWT_ACCESS_TOKEN_EXPIRES", 86400))  # 1 day
    JWT_REFRESH_TOKEN_EXPIRES = int(os.getenv("JWT_REFRESH_TOKEN_EXPIRES", 86400))  # 1 day
    JWT_BLACKLIST_ENABLED = True
    REDIS_HOST=REDIS_HOST
