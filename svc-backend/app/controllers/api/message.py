from datetime import timedelta
import logging
import os
import re
from sqlalchemy import and_
from app.configs import Config
from app.libs.utils import (
    log_deb,
    log_rep,
    log_err,
    get_project_root,
    handle_duplicate,
)
from flask_restful import request
from flask_jwt_extended import (
    get_jwt_identity,
    jwt_required,
)
from flask_restful import Resource, marshal_with, reqparse, marshal
from app.controllers import message_pagination, message_detail_fields, message_fields
from app import db
from app.models.message import Message
from app.models.user import User
from app.helpers.rest import response

KEY_REQUEST_EMAIL_VERIFICATION = "request-email-verification"

JWT_PASSWORD_RESET_EXPIRES_HOURS = 24
JWT_PASSWORD_RESET_EXPIRES = timedelta(hours=JWT_PASSWORD_RESET_EXPIRES_HOURS)

IMG_ALLOWED_EXTENSIONS = os.environ.get("IMG_ALLOWED_EXTENSIONS") or [
    ".jpg",
    ".jpeg",
    ".png",
    ".gif",
]
MAX_CONTENT_LENGTH = os.environ.get("MAX_CONTENT_LENGTH") or 1024 * 1024
IMG_UPLOAD_PATH = os.environ.get("IMG_UPLOAD_PATH") or os.path.join(
    get_project_root(), "upload", "message"
)

logger = logging.getLogger(__name__)


class MessagesList(Resource):
    @jwt_required
    # @marshal_with(message_pagination)
    def get(self):
        page = request.args.get("page")
        page_size = request.args.get("page_size")

        identity = get_jwt_identity()

        if not page:
            page = 1
        if not page_size:
            page_size = 5

        messages = (
            Message()
            .query.filter(
                and_(
                    and_(Message.is_active == True, Message.user_id == identity["id"]),
                    Message.parent_id == None,
                )
            )
            .order_by(Message.created_at.desc())  # noqa: E712
            .order_by(Message.updated_at.desc())
            .paginate(int(page), int(page_size), False)
        )

        data = {
            "data": messages.items,
            "page": messages.page,
            "page_size": messages.per_page,
            "total_pages": messages.pages,
            "total": messages.total,
            "next_page": messages.next_num,
            "prev_page": messages.prev_num,
        }
        return response(
            200,
            data=marshal(data, message_pagination),
            message="Berhasil mendapatkan data message",
        )


class MessagesListByUserID(Resource):
    @jwt_required
    # @marshal_with(message_pagination)
    def get(self, id):
        page = request.args.get("page")
        page_size = request.args.get("page_size")

        if not page:
            page = 1
        if not page_size:
            page_size = 5

        messages = (
            Message()
            .query.filter(
                and_(
                    and_(
                        Message.is_active == True,
                        Message.user_id == id,
                    ),
                    Message.parent_id == None,
                )
            )
            .order_by(Message.created_at.desc())  # noqa: E712
            .order_by(Message.updated_at.desc())
            .paginate(int(page), int(page_size), False)
        )
        data = {
            "data": messages.items,
            "page": messages.page,
            "page_size": messages.per_page,
            "total_pages": messages.pages,
            "total": messages.total,
            "next_page": messages.next_num,
            "prev_page": messages.prev_num,
        }
        return response(
            200,
            data=marshal(data, message_pagination),
            message="Berhasil mendapatkan data message",
        )


class MessageByID(Resource):
    @jwt_required
    # @marshal_with(message_detail_fields)
    def get(self, id):
        page = request.args.get("comment_page")

        if not page:
            page = 1
        message = Message.query.get(id)

        comments = (
            Message.query.filter(Message.parent_id == id)
            .order_by(Message.created_at.asc())  # noqa: E712
            .order_by(Message.updated_at.asc())
            .paginate(int(page), int(5), False)
        )
        data = {
            "id": message.id,
            "content": message.content,
            "user": message.user,
            "is_active": message.is_active,
            "created_at": message.created_at,
            "updated_at": message.updated_at,
            "comments": {
                "data": comments.items,
                "page": comments.page,
                "page_size": comments.per_page,
                "total_pages": comments.pages,
                "total": comments.total,
                "next_page": comments.next_num,
                "prev_page": comments.prev_num,
            },
        }
        return response(
            200,
            data=marshal(data, message_detail_fields),
            message="Berhasil mendapatkan detail message",
        )


class Messages(Resource):
    parser_post = reqparse.RequestParser()
    parser_post.add_argument("content", required=True, location="json")
    parser_post.add_argument("parent_id", required=False, location="json")

    @jwt_required
    def post(self):
        data = Messages.parser_post.parse_args()
        identity = get_jwt_identity()
        user = User.query.get(identity["id"])
        if not user:
            return {"message": "User not found!"}, 400

        message_data = Message()
        message_data.content = data["content"]
        message_data.user_id = user.id
        if data["parent_id"]:
            message_data.parent_id = data["parent_id"]

        try:
            db.session.add(message_data)
            db.session.commit()
        except Exception as e:
            if "psycopg2.errors.UniqueViolation" in e.args[0]:
                return handle_duplicate(e), 409
            raise (e)

        return response(
            200,
            data=marshal(message_data, message_fields),
            message="Berhasil membuat message/comment",
        )
