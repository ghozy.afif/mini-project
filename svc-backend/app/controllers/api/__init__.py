from flask import Blueprint
from flask_restful import Api
from .health import *
from .example_api import *
from .user import *
from .message import *

api_blueprint = Blueprint("api", __name__, url_prefix='/api')
api = Api(api_blueprint)

api.add_resource(HealthCheck, "/health")
api.add_resource(ExampleApi, "/example/list")
api.add_resource(ExampleAPIById, "/example/list/<id>")
api.add_resource(ExampleApiDelete, "/example/delete/<id>")
api.add_resource(ExampleApiInsert, "/example/add")
api.add_resource(ExampleApiUpdate, "/example/edit")

api.add_resource(UserList, "/users")
api.add_resource(UserProfile, "/user")
api.add_resource(UserPropicGet, "/user/propic/<filename>")
api.add_resource(UserByID, "/user/<id>")
api.add_resource(Login, "/login")
api.add_resource(Logout, "/logout")
api.add_resource(LogoutAllDevice, "/force-logout")
api.add_resource(Register, "/register")
api.add_resource(TokenRefresh, "/token/refresh")

api.add_resource(Messages, "/message")
api.add_resource(MessageByID, "/message/<id>")
api.add_resource(MessagesList, "/messages")
api.add_resource(MessagesListByUserID, "/messages/<id>")



