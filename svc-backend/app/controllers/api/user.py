from datetime import timedelta, datetime
import imghdr
import logging
import os
from pathlib import Path
import re
from sqlalchemy import and_
from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filename
from app.configs import Config
from app.libs.utils import (
    log_deb,
    log_rep,
    log_err,
    get_project_root,
    parse_boolean,
    handle_duplicate,
    random_string,
)
from flask import send_from_directory
from flask_restful import request
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    get_jwt_identity,
    get_jti,
    jwt_required,
    jwt_refresh_token_required,
    get_raw_jwt,
    # get_jwt,
)
from flask_restful import Resource, marshal, marshal_with, reqparse
from app.controllers import user_fields, user_pagination
from app import db, redis_con
from app.models.user import User
from app.models.active_token import ActiveToken
from app.helpers.rest import response

KEY_REQUEST_EMAIL_VERIFICATION = "request-email-verification"

JWT_PASSWORD_RESET_EXPIRES_HOURS = 24
JWT_PASSWORD_RESET_EXPIRES = timedelta(hours=JWT_PASSWORD_RESET_EXPIRES_HOURS)

IMG_ALLOWED_EXTENSIONS = os.environ.get("IMG_ALLOWED_EXTENSIONS") or [
    ".jpg",
    ".jpeg",
    ".png",
    ".gif",
]
MAX_CONTENT_LENGTH = os.environ.get("MAX_CONTENT_LENGTH") or 1024 * 1024
IMG_UPLOAD_PATH = os.environ.get("IMG_UPLOAD_PATH") or os.path.join(
    get_project_root(), "upload", "propic"
)

logger = logging.getLogger(__name__)


class UserList(Resource):
    @jwt_required
    # @marshal_with(user_pagination)
    def get(self):
        logger.debug("Enter Userlist")
        page = request.args.get("page")
        page_size = request.args.get("page_size")

        if not page:
            page = 1
        if not page_size:
            page_size = 5
        users = (
            User.query.filter(User.is_active == True)
            .order_by(User.created_at.desc())  # noqa: E712
            .order_by(User.updated_at.desc())
            .paginate(int(page), int(page_size), False)
        )

        data = {
            "data": users.items,
            "page": users.page,
            "page_size": users.per_page,
            "total_pages": users.pages,
            "total": users.total,
            "next_page": users.next_num,
            "prev_page": users.prev_num,
        }
        return response(
            200,
            data=marshal(data, user_pagination),
            message="Berhasil mendapatkan data user",
        )


class UserPropicGet(Resource):
    def get(self, filename):
        path = Path(IMG_UPLOAD_PATH)
        return send_from_directory(str(path.absolute()), filename)


class UserProfile(Resource):
    @jwt_required
    # @marshal_with(user_fields)
    def get(self):
        identity = get_jwt_identity()
        user = User.query.get(identity["id"])
        return response(
            200,
            data=marshal(user, user_fields),
            message="Berhasil mendapatkan data detail user",
        )

    parser_put = reqparse.RequestParser()
    parser_put.add_argument("username", location="form", required=True)
    parser_put.add_argument("email", location="form", required=True)
    parser_put.add_argument("first_name", location="form", required=True)
    parser_put.add_argument("last_name", location="form", required=True)
    parser_put.add_argument("pic", type=FileStorage, location="files")

    @jwt_required
    def put(self):
        identity = get_jwt_identity()
        user = User.query.get(identity["id"])

        data = UserProfile.parser_put.parse_args()

        # Email validation
        if user.email != data["email"]:
            if not User.is_email_valid(data["email"]):
                return {"message": "Email is invalid"}, 400
            if User.find_by_email(data["email"]):
                return {"message": "Email has already been used"}, 400

            user.email = data["email"]

        # Username validation
        username = data["username"].strip()
        if not username:
            return {"message": "Username cannot be empty"}, 400
        if user.username != username:
            if User.find_by_username(username):
                return {"message": "Username has already been used"}, 400
            user.username = username

        # Name validation
        user.first_name = " ".join(data["first_name"].split())
        user.last_name = " ".join(data["last_name"].split())
        if not user.name:
            return {"message": "Name cannot be empty"}, 400

        # propic
        if not os.path.isdir(IMG_UPLOAD_PATH):
            os.mkdir(IMG_UPLOAD_PATH)

        uploaded_file = data["pic"]
        filename = (
            secure_filename(uploaded_file.filename)
            if uploaded_file is not None
            else None
        )

        if filename != "" and filename is not None:
            file_ext = self.validate_image(uploaded_file.stream)

            if file_ext not in IMG_ALLOWED_EXTENSIONS:
                return {"message": "File is not allowed"}, 422

            propic_name = user.propic
            if propic_name is not None:
                path = Path(IMG_UPLOAD_PATH + "/" + propic_name)
                try:
                    os.remove(str(path.absolute()))
                except Exception:
                    logger.info("file not exist")

            random_filename = random_string() + file_ext
            user.propic = random_filename

            path = Path(IMG_UPLOAD_PATH + "/" + random_filename)
            uploaded_file.save(str(path.absolute()))
        db.session.commit()
        return response(
            200,
            data=marshal(user, user_fields),
            message="Berhasil melakukan update data user",
        )

    def validate_image(self, stream):
        header = stream.read(512)
        stream.seek(0)
        format = imghdr.what(None, header)

        if not format:
            return None
        return "." + format.lower()


class UserByID(Resource):
    @jwt_required
    # @marshal_with(user_fields)
    def get(self, id):
        user = User.query.get(id)
        return response(
            200,
            data=marshal(user, user_fields),
            message="Berhasil mendapatkan data user",
        )


class Login(Resource):
    parser_post = reqparse.RequestParser()
    parser_post.add_argument("username", location=["json", "form"], required=True)
    parser_post.add_argument("password", location=["json", "form"], required=True)
    parser_post.add_argument("remember_me", location=["json", "form"], required=False)

    def post(self):
        data = Login.parser_post.parse_args()

        username = data.username
        password = data.password
        remember_me = parse_boolean(data["remember_me"])

        user = {}
        if User.is_email_valid(data.username):
            user = User.find_by_email(username)
        else:
            user = User.find_by_username(username)

        if not user:
            return {"message": f"User '{username}' doesn't exist"}, 401

        if user.verify_password(password):
            identity = {
                "id": user.id,
                "username": username,
                "email": user.email,
                "full_name": user.full_name,
            }

            access_token = create_access_token(identity=identity)
            if remember_me:
                refresh_token = create_refresh_token(
                    identity=identity, expires_delta=False
                )
            else:
                refresh_token = create_refresh_token(identity=identity)

            # longest expire time
            expire_date = datetime.now() + timedelta(
                seconds=Config.JWT_REFRESH_TOKEN_EXPIRES
                + Config.JWT_ACCESS_TOKEN_EXPIRES
            )
            active_token = ActiveToken(
                refresh_jti=get_jti(refresh_token),
                access_jti=get_jti(access_token),
                user_id=user.id,
                can_expire=not remember_me,
                expire_date=expire_date,
            )

            try:
                db.session.add(active_token)
                db.session.commit()
            except Exception as e:
                log_err("Cannot add active token")
                log_err(e, exc_info=True)
                return {"message": "Something went wrong"}, 500

            try:
                user.last_login = datetime.now()
                db.session.commit()
            except Exception as e:
                log_err(e, exc_info=True)
                return {"message": "Cannot update last login user"}, 500

            resp = {
                "message": f"Logged in as '{username}'",
                "access_token": access_token,
                "refresh_token": refresh_token,
                "remember_me": remember_me,
                "user": marshal(user, user_fields),
            }
            return response(
                200,
                data=resp,
                message="Berhasil mendapatkan akses login",
            )

        else:
            return {"message": "Wrong credentials"}, 401


class Logout(Resource):
    # Endpoint for revoking the current users access token. Save the JWTs unique
    # identifier (jti) in redis. Also set a Time to Live (TTL)  when storing the JWT
    # so that it will automatically be cleared out of redis after the token expires.
    @jwt_required
    def post(self):
        jti = get_raw_jwt()["jti"]
        exp = get_raw_jwt()["exp"]
        try:
            ActiveToken.query.filter_by(access_jti=jti).delete()
            if not redis_con.get(jti):
                redis_con.set(jti, jti, exat=exp)
            db.session.commit()
            return response(
                200,
                data={"message": "Access token has been revoked"},
                message="Berhasil mendapatkan akses logout",
            )
        except Exception as e:
            logger.error(e, exc_info=True)
            return {"message": "Something went wrong"}, 500


class LogoutAllDevice(Resource):
    # Endpoint for revoking the current users access token. Save the JWTs unique
    # identifier (jti) in redis. Also set a Time to Live (TTL)  when storing the JWT
    # so that it will automatically be cleared out of redis after the token expires.
    @jwt_required
    def post(self):
        user = User.query.get(get_jwt_identity()["id"])
        try:
            user.revoke_all_active_tokens()
            return response(
                200,
                data={"message": "All access token has been revoked"},
                message="Berhasil mendapatkan akses logout pada seluruh device",
            )
        except Exception as e:
            logger.error(e, exc_info=True)
            return {"message": "Something went wrong"}, 500


class TokenRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        refresh_jti = get_raw_jwt()["jti"]
        current_user = get_jwt_identity()

        try:
            active_token = ActiveToken.query.get(refresh_jti)
            access_token = create_access_token(identity=current_user)
            active_token.access_jti = get_jti(access_token)
            db.session.commit()
        except Exception as e:
            logger.error(e, exc_info=True)
            return {"message": "Something went wrong"}, 500
        return response(
            200,
            data={"access_token": access_token},
            message="Berhasil membuat access_token baru",
        )


class Register(Resource):
    parser_post = reqparse.RequestParser()
    parser_post.add_argument("username", required=True, location="json")
    parser_post.add_argument("email", required=True, location="json")
    parser_post.add_argument("password", required=True, location="json")
    parser_post.add_argument("first_name", required=True, location="json")
    parser_post.add_argument("last_name", required=True, location="json")

    def post(self):
        data = Register.parser_post.parse_args()

        if not User.is_email_valid(data["email"]):
            return {"message": "Email is invalid"}, 400

        if not User.is_password_valid(data["password"]):
            return {"message": "Password must consist of 5-32 characters"}, 400

        salt, hashed_password = User.generate_salt_and_hash(data["password"])

        user = User(
            username=data["username"],
            email=data["email"],
            first_name=data["first_name"],
            last_name=data["last_name"],
            password=hashed_password,
            salt=salt,
        )

        try:
            db.session.add(user)
            db.session.commit()
        except Exception as e:
            if "psycopg2.errors.UniqueViolation" in e.args[0]:
                return handle_duplicate(e), 409
            raise (e)
        return response(
            200,
            data=marshal(user, user_fields),
            message="Berhasil melakukan registrasi",
        )
