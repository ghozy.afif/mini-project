import traceback

import pytz
from flask_restful import fields

from app import db
from app.libs.utils import handle_duplicate

user_fields = {
    "id": fields.Integer,
    "username": fields.String,
    "email": fields.String,
    "first_name": fields.String,
    "last_name": fields.String,
    "full_name": fields.String,
    "propic": fields.String,
    "is_active": fields.Boolean,
    "created_at": fields.DateTime,
    "updated_at": fields.DateTime,
}

user_fields_list = {"users": fields.List(fields.Nested(user_fields))}
user_pagination = {
    "data": fields.List(fields.Nested(user_fields)),
    "page": fields.Integer,
    "page_size": fields.Integer,
    "total_pages": fields.Integer,
    "total": fields.Integer,
    "next_page": fields.Integer,
    "prev_page": fields.Integer,
}

message_fields = {
    "id": fields.Integer,
    "content": fields.String,
    "user": fields.Nested(
        {
            "id": fields.Integer,
            "full_name": fields.String,
            "username": fields.String,
        }
    ),
    "is_active": fields.Boolean,
    "created_at": fields.DateTime,
    "updated_at": fields.DateTime,
}

message_fields["comments"] = fields.List(fields.Nested(message_fields))

message_fields_list = {"messages": fields.List(fields.Nested(message_fields))}
message_pagination = {
    "data": fields.List(fields.Nested(message_fields)),
    "page": fields.Integer,
    "page_size": fields.Integer,
    "total_pages": fields.Integer,
    "total": fields.Integer,
    "next_page": fields.Integer,
    "prev_page": fields.Integer,
}

message_detail_fields = {
    "id": fields.Integer,
    "content": fields.String,
    "user": fields.Nested(
        {
            "id": fields.Integer,
            "full_name": fields.String,
            "username": fields.String,
        }
    ),
    "is_active": fields.Boolean,
    "created_at": fields.DateTime,
    "updated_at": fields.DateTime,
    "comments": fields.Nested(message_pagination),
}


def db_commit():
    try:
        db.session.commit()
    except Exception as e:
        if "psycopg2.errors.UniqueViolation" in e.args[0]:
            return {"message": handle_duplicate(e)}, 409
        traceback.print_exc()
        return {"message": "Something went wrong"}, 500

    return {"message": "OK"}, 200


from .api import api_blueprint
