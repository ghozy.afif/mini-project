from asyncio.log import logger
from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager
from flask_migrate import Migrate
import redis
from . import configs

app = Flask(__name__)
app.config.from_object(configs.Config)
CORS(app, resources={r"/api/*": {"origins": "*"}})
db = SQLAlchemy(app)
migrate = Migrate(app, db)
jwt = JWTManager(app)

# Setup our redis connection for storing the blocklisted tokens. You will probably
# want your redis instance configured to persist data to disk, so that a restart
# does not cause your application to forget that a JWT was revoked.
redis_con = redis.StrictRedis(host=configs.Config.REDIS_HOST, port=6379, db=0, decode_responses=True)

# Callback function to check if a JWT exists in the redis blocklist
@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token["jti"]
    token_in_redis = redis_con.get(jti)
    return token_in_redis is not None


from .controllers import api_blueprint

app.register_blueprint(api_blueprint)

# import all models
from .models.active_token import *
from .models.user import *
from .models.message import *

@app.before_first_request
def create_tables():
    db.create_all()
    pass

def create_app():
    return app
